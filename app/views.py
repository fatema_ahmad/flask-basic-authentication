from app import app
from flask import render_template,request,redirect,jsonify,make_response
from datetime import datetime

@app.route("/register", methods=["POST"])
def json_example():
    if request.is_json:
        req = request.get_json()
        resp = {
            "message": "JSON received!",
            "username": req.get("username"),
            "email": req.get("email"),
            "password": req.get("password")
        }
        res = make_response(jsonify(resp), 200)
        return res
    else:
        return make_response(jsonify({"message": "Request body must be JSON"}), 400)
